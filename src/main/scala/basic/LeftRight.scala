package basic
import processing.core._
import processing.core.PConstants._
import scala.util.Random

class LeftRight extends MyPApplet{

  val lines = new Lines(this)
  val rect1 = new MyRectangle(this)
  
  var n : Int = 0
  
  def foo(pn : Int) = {
    n = pn
  }
  
  
  override def setup() = {
    size(640, 360) // Size must be the first statement
    frameRate(300)
    stroke(255) // Set line drawing color to white

    lines.setup()
    rect1.setup()
  }

  override def draw() = {
    lines.draw()
    rect1.draw()
  }
  
  override def keyPressed() = {
    //println(key.toInt)
    if (key == CODED) {
	    if (keyCode == LEFT) {
	      println("LEFT")
	      rect1.left()
	    }
	    if (keyCode == RIGHT) {
	      println("RIGHT")
	      rect1.right()
	    }
    }
  }
}

class Lines(canvas: LeftRight) {
  
  var y = 100
  var up = true

  def setup() = {
    //canvas.frameRate(90)
  }

  def draw() = {
    //println("Box: draw()")
    canvas.background(0) // Clear the screen with a black background
    if (up) {
      y = y - 1
    } else {
      y = y + 1
    }
    if (y < 0) {
      up = false
    } else if (y == canvas.height) {
      up = true
    }
    canvas.line(0, y, canvas.width, y)

  }
}

class MyRectangle(canvas: LeftRight) {
  val delta = 5
  var xpos = 1
  
  def setup() = {
    
  }
  def draw() = {
    canvas.rect(xpos,10,100,200)
  }
  
  def left() = {
    if(xpos > 0) {
      xpos -= delta
    }
  }
  
  def right() = {
    if(xpos < canvas.width) {
      xpos += delta
    }
  }
  
}
